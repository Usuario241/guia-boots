$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
            interval: 1500
    });

    $('#contacto').on('show.bs.modal', function(e){
        console.log('En el modal se esta mostrando');
        $('#contactoBtn').removeClass('btn-outline-success');
        $('#contactoBtn').addClass('btn-primary');
        $('#contactoBtn').prop('disable',true);
    })

    $('#contacto').on('shown.bs.modal', function(e){
        console.log('En el modal se mostro');
    })

    $('#contacto').on('hide.bs.modal', function(e){
        console.log('En el modal se oculta');	
    })

    $('#contacto').on('hidden.bs.modal', function(e){
        console.log('En el modal se Ocuto');
        $('#contactoBtn').prop('disable',false);
    })
});
