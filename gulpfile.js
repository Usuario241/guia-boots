'use strict' 

 

var gulp = require('gulp'), 
    sass = require('gulp-sass'), 
    browserSync = require('browser-sync'), 
    del = require('del'), 
    imagemin = require('gulp-imagemin'), 
    uglify = require('gulp-uglify'), 
    usemin= require('gulp-usemin'), 
    rev = require('gulp-rev'), 
    cleancss = require('gulp-clean-css'), 
    flatmap = require('gulp-flatmap'), 
    htmlmin = require('gulp-htmlmin'); 
 

gulp.task('sass', function () { 
    return gulp.src('./css/*.scss') 
        .pipe(sass().on('error', sass.logError)) 
        .pipe(gulp.dest('./css')); 
}); 

 
